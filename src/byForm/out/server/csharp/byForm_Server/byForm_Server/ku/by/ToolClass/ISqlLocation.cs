using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace byForm_Server.ku.by.ToolClass
{
    public interface ISqlLocation
    {
        System.Collections.Generic.Dictionary<string, string> SqlLocationDic { get; }
    }
}
