using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace byForm_Server.ku.byForm.Enum
{
    public enum memberMode
    {
        structureForm,

        structureData64,

        structureData256,

        structureData1024,

        structureData4000,
    }
}
