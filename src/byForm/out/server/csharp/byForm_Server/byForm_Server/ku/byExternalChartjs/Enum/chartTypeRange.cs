using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace byForm_Server.ku.byExternalChartjs.Enum
{
    public enum chartTypeRange
    {
        all,

        general,

        generalRadar,

        scatterRadar,

        linearBubble,
    }
}
