package $Ku.byExternalChartjs.Enum;

public enum chartType{
    bar,
    bubble,
    doughnut,
    pie,
    line,
    scatter,
    polarArea,
    radar,
    linear
}