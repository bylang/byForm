package $Ku.by.Enum;

public enum DockStyle{
    none,
    top,
    bottom,
    left,
    right,
    fill
}