package $Ku.by.ToolClass.Sql;

public interface IFields{
    int getFieldCount() ;
    java.util.ArrayList<$Ku.by.ToolClass.Sql.AbstractSelectField> getFieldList() ;
    void setFieldList(java.util.ArrayList<$Ku.by.ToolClass.Sql.AbstractSelectField> value) ;
}
