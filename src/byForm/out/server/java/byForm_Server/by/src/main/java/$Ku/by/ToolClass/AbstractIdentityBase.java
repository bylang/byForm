package $Ku.by.ToolClass;

public abstract class AbstractIdentityBase {
    public String ku;
    public Object to;

    public AbstractIdentityBase() {
    }


    public String getKu() {
        return ku;
    }

    public void setKu(String ku) {
        this.ku = ku;
    }

    public Object getTo() {
        return to;
    }

    public void setTo(Object to) {
        this.to = to;
    }

    public void $setProps() {
    }
}
