package $Ku.by.ToolClass;

public class Entry<K, V> {
    public K k;
    public V v;

    public Entry(K k, V v) {
        this.k = k;
         this.v = v;
    }
}
