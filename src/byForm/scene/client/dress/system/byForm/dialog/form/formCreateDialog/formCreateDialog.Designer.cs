﻿/* 本文件由拜语言 IDE 自动生成，网址:http://www.baiyuyan.com/  生成时间:2024-04-30 02:43:05  */
namespace byForm.dress.form
{
    partial class formCreateDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headPanel = new System.Windows.Forms.Panel();
            this.headLabel = new System.Windows.Forms.Label();
            this.bodyPanel = new System.Windows.Forms.Panel();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.namePanel = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameInputBox = new System.Windows.Forms.TextBox();
            this.summaryPanel = new System.Windows.Forms.Panel();
            this.summaryLabel = new System.Windows.Forms.Label();
            this.summaryInputBox = new System.Windows.Forms.TextBox();
            this.buttonContainer = new System.Windows.Forms.Panel();
            this.createButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.headPanel.SuspendLayout();
            this.bodyPanel.SuspendLayout();
            this.contentPanel.SuspendLayout();
            this.namePanel.SuspendLayout();
            this.summaryPanel.SuspendLayout();
            this.buttonContainer.SuspendLayout();
            this.SuspendLayout();
            //
            //  headPanel
            //
            this.headPanel.Location = new System.Drawing.Point(4, 4);
            this.headPanel.Name =  "headPanel";
            this.headPanel.Size = new System.Drawing.Size(100, 30);
            this.headPanel.TabIndex =  0;
            //
            //  headLabel
            //
            this.headLabel.AutoSize = true;
            this.headLabel.Location = new System.Drawing.Point(112, 4);
            this.headLabel.Name = "headLabel";
            this.headLabel.Size = new System.Drawing.Size(100,30);
            this.headLabel.TabIndex = 0;
            this.headLabel.Text = "标题标签";
            //
            //  bodyPanel
            //
            this.bodyPanel.Location = new System.Drawing.Point(220, 4);
            this.bodyPanel.Name =  "bodyPanel";
            this.bodyPanel.Size = new System.Drawing.Size(100, 30);
            this.bodyPanel.TabIndex =  0;
            //
            //  contentPanel
            //
            this.contentPanel.Location = new System.Drawing.Point(328, 4);
            this.contentPanel.Name =  "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(100, 30);
            this.contentPanel.TabIndex =  0;
            //
            //  namePanel
            //
            this.namePanel.Location = new System.Drawing.Point(436, 4);
            this.namePanel.Name =  "namePanel";
            this.namePanel.Size = new System.Drawing.Size(100, 30);
            this.namePanel.TabIndex =  0;
            //
            //  nameLabel
            //
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(544, 4);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(100,30);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "表单标题名称标签";
            //
            //  nameInputBox
            //
            this.nameInputBox.Location = new System.Drawing.Point(652, 4);
            this.nameInputBox.Name = "nameInputBox";
            this.nameInputBox.Size = new System.Drawing.Size(100, 30);
            this.nameInputBox.TabIndex = 0;
            //
            //  summaryPanel
            //
            this.summaryPanel.Location = new System.Drawing.Point(760, 4);
            this.summaryPanel.Name =  "summaryPanel";
            this.summaryPanel.Size = new System.Drawing.Size(100, 30);
            this.summaryPanel.TabIndex =  0;
            //
            //  summaryLabel
            //
            this.summaryLabel.AutoSize = true;
            this.summaryLabel.Location = new System.Drawing.Point(4, 42);
            this.summaryLabel.Name = "summaryLabel";
            this.summaryLabel.Size = new System.Drawing.Size(100,30);
            this.summaryLabel.TabIndex = 0;
            this.summaryLabel.Text = "表单说明标签";
            //
            //  summaryInputBox
            //
            this.summaryInputBox.Location = new System.Drawing.Point(112, 42);
            this.summaryInputBox.Name = "summaryInputBox";
            this.summaryInputBox.Size = new System.Drawing.Size(100, 30);
            this.summaryInputBox.TabIndex = 0;
            //
            //  buttonContainer
            //
            this.buttonContainer.Location = new System.Drawing.Point(220, 42);
            this.buttonContainer.Name =  "buttonContainer";
            this.buttonContainer.Size = new System.Drawing.Size(100, 30);
            this.buttonContainer.TabIndex =  0;
            //
            //  createButton
            //
            this.createButton.Location = new System.Drawing.Point(328, 42);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(100, 30);
            this.createButton.TabIndex = 0;
            this.createButton.Text = "创建-按钮";
            this.createButton.UseVisualStyleBackColor = true; 
            //
            //  cancelButton
            //
            this.cancelButton.Location = new System.Drawing.Point(436, 42);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(100, 30);
            this.cancelButton.TabIndex = 0;
            this.cancelButton.Text = "取消-按钮";
            this.cancelButton.UseVisualStyleBackColor = true; 
            //
            //  formCreateDialog
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.buttonContainer);
            this.Controls.Add(this.summaryInputBox);
            this.Controls.Add(this.summaryLabel);
            this.Controls.Add(this.summaryPanel);
            this.Controls.Add(this.nameInputBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.namePanel);
            this.Controls.Add(this.contentPanel);
            this.Controls.Add(this.bodyPanel);
            this.Controls.Add(this.headLabel);
            this.Controls.Add(this.headPanel);
            this.Name = "formCreateDialog";
            this.Text = "创建新表单的弹窗";
            this.ResumeLayout(false);
            this.PerformLayout();
            this.headPanel.ResumeLayout(false);
            this.headPanel.PerformLayout();
            this.bodyPanel.ResumeLayout(false);
            this.bodyPanel.PerformLayout();
            this.contentPanel.ResumeLayout(false);
            this.contentPanel.PerformLayout();
            this.namePanel.ResumeLayout(false);
            this.namePanel.PerformLayout();
            this.summaryPanel.ResumeLayout(false);
            this.summaryPanel.PerformLayout();
            this.buttonContainer.ResumeLayout(false);
            this.buttonContainer.PerformLayout();


        }
        
        #endregion

        private System.Windows.Forms.Panel headPanel;
        private System.Windows.Forms.Label headLabel;
        private System.Windows.Forms.Panel bodyPanel;
        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.Panel namePanel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameInputBox;
        private System.Windows.Forms.Panel summaryPanel;
        private System.Windows.Forms.Label summaryLabel;
        private System.Windows.Forms.TextBox summaryInputBox;
        private System.Windows.Forms.Panel buttonContainer;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Button cancelButton;

        
    }
}