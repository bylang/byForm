﻿/* 本文件由拜语言 IDE 自动生成，网址:http://www.baiyuyan.com/  生成时间:2024-04-30 02:43:05  */
namespace byUser.dress.user
{
    partial class diUserReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cLblName = new System.Windows.Forms.Label();
            this.cTxtName = new System.Windows.Forms.TextBox();
            this.cLblPwd = new System.Windows.Forms.Label();
            this.cTxtPwd = new System.Windows.Forms.TextBox();
            this.cLblConfirmPwd = new System.Windows.Forms.Label();
            this.cTxtConfirmPwd = new System.Windows.Forms.TextBox();
            this.cLblMobile = new System.Windows.Forms.Label();
            this.cTxtMobile = new System.Windows.Forms.TextBox();
            this.cLblMail = new System.Windows.Forms.Label();
            this.cTxtMail = new System.Windows.Forms.TextBox();
            this.cLblSafetyCode = new System.Windows.Forms.Label();
            this.cTxtSafetyCode = new System.Windows.Forms.TextBox();
            this.cLblSendCode = new System.Windows.Forms.Label();
            this.cBtnReg = new System.Windows.Forms.Button();
            this.cBtnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            //
            //  cLblName
            //
            this.cLblName.AutoSize = true;
            this.cLblName.Location = new System.Drawing.Point(4, 4);
            this.cLblName.Name = "cLblName";
            this.cLblName.Size = new System.Drawing.Size(100,30);
            this.cLblName.TabIndex = 0;
            this.cLblName.Text = "用户名";
            //
            //  cTxtName
            //
            this.cTxtName.Location = new System.Drawing.Point(112, 4);
            this.cTxtName.Name = "cTxtName";
            this.cTxtName.Size = new System.Drawing.Size(100, 30);
            this.cTxtName.TabIndex = 0;
            //
            //  cLblPwd
            //
            this.cLblPwd.AutoSize = true;
            this.cLblPwd.Location = new System.Drawing.Point(220, 4);
            this.cLblPwd.Name = "cLblPwd";
            this.cLblPwd.Size = new System.Drawing.Size(100,30);
            this.cLblPwd.TabIndex = 0;
            this.cLblPwd.Text = "密码";
            //
            //  cTxtPwd
            //
            this.cTxtPwd.Location = new System.Drawing.Point(328, 4);
            this.cTxtPwd.Name = "cTxtPwd";
            this.cTxtPwd.Size = new System.Drawing.Size(100, 30);
            this.cTxtPwd.TabIndex = 0;
            //
            //  cLblConfirmPwd
            //
            this.cLblConfirmPwd.AutoSize = true;
            this.cLblConfirmPwd.Location = new System.Drawing.Point(436, 4);
            this.cLblConfirmPwd.Name = "cLblConfirmPwd";
            this.cLblConfirmPwd.Size = new System.Drawing.Size(100,30);
            this.cLblConfirmPwd.TabIndex = 0;
            this.cLblConfirmPwd.Text = "再输入一遍";
            //
            //  cTxtConfirmPwd
            //
            this.cTxtConfirmPwd.Location = new System.Drawing.Point(544, 4);
            this.cTxtConfirmPwd.Name = "cTxtConfirmPwd";
            this.cTxtConfirmPwd.Size = new System.Drawing.Size(100, 30);
            this.cTxtConfirmPwd.TabIndex = 0;
            //
            //  cLblMobile
            //
            this.cLblMobile.AutoSize = true;
            this.cLblMobile.Location = new System.Drawing.Point(652, 4);
            this.cLblMobile.Name = "cLblMobile";
            this.cLblMobile.Size = new System.Drawing.Size(100,30);
            this.cLblMobile.TabIndex = 0;
            this.cLblMobile.Text = "手机";
            //
            //  cTxtMobile
            //
            this.cTxtMobile.Location = new System.Drawing.Point(760, 4);
            this.cTxtMobile.Name = "cTxtMobile";
            this.cTxtMobile.Size = new System.Drawing.Size(100, 30);
            this.cTxtMobile.TabIndex = 0;
            //
            //  cLblMail
            //
            this.cLblMail.AutoSize = true;
            this.cLblMail.Location = new System.Drawing.Point(4, 42);
            this.cLblMail.Name = "cLblMail";
            this.cLblMail.Size = new System.Drawing.Size(100,30);
            this.cLblMail.TabIndex = 0;
            this.cLblMail.Text = "邮箱";
            //
            //  cTxtMail
            //
            this.cTxtMail.Location = new System.Drawing.Point(112, 42);
            this.cTxtMail.Name = "cTxtMail";
            this.cTxtMail.Size = new System.Drawing.Size(100, 30);
            this.cTxtMail.TabIndex = 0;
            //
            //  cLblSafetyCode
            //
            this.cLblSafetyCode.AutoSize = true;
            this.cLblSafetyCode.Location = new System.Drawing.Point(220, 42);
            this.cLblSafetyCode.Name = "cLblSafetyCode";
            this.cLblSafetyCode.Size = new System.Drawing.Size(100,30);
            this.cLblSafetyCode.TabIndex = 0;
            this.cLblSafetyCode.Text = "安全码";
            //
            //  cTxtSafetyCode
            //
            this.cTxtSafetyCode.Location = new System.Drawing.Point(328, 42);
            this.cTxtSafetyCode.Name = "cTxtSafetyCode";
            this.cTxtSafetyCode.Size = new System.Drawing.Size(100, 30);
            this.cTxtSafetyCode.TabIndex = 0;
            //
            //  cLblSendCode
            //
            this.cLblSendCode.AutoSize = true;
            this.cLblSendCode.Location = new System.Drawing.Point(436, 42);
            this.cLblSendCode.Name = "cLblSendCode";
            this.cLblSendCode.Size = new System.Drawing.Size(100,30);
            this.cLblSendCode.TabIndex = 0;
            this.cLblSendCode.Text = "发送安全码";
            //
            //  cBtnReg
            //
            this.cBtnReg.Location = new System.Drawing.Point(544, 42);
            this.cBtnReg.Name = "cBtnReg";
            this.cBtnReg.Size = new System.Drawing.Size(100, 30);
            this.cBtnReg.TabIndex = 0;
            this.cBtnReg.Text = "注册";
            this.cBtnReg.UseVisualStyleBackColor = true; 
            //
            //  cBtnCancel
            //
            this.cBtnCancel.Location = new System.Drawing.Point(652, 42);
            this.cBtnCancel.Name = "cBtnCancel";
            this.cBtnCancel.Size = new System.Drawing.Size(100, 30);
            this.cBtnCancel.TabIndex = 0;
            this.cBtnCancel.Text = "取消";
            this.cBtnCancel.UseVisualStyleBackColor = true; 
            //
            //  diUserReg
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.cBtnCancel);
            this.Controls.Add(this.cBtnReg);
            this.Controls.Add(this.cLblSendCode);
            this.Controls.Add(this.cTxtSafetyCode);
            this.Controls.Add(this.cLblSafetyCode);
            this.Controls.Add(this.cTxtMail);
            this.Controls.Add(this.cLblMail);
            this.Controls.Add(this.cTxtMobile);
            this.Controls.Add(this.cLblMobile);
            this.Controls.Add(this.cTxtConfirmPwd);
            this.Controls.Add(this.cLblConfirmPwd);
            this.Controls.Add(this.cTxtPwd);
            this.Controls.Add(this.cLblPwd);
            this.Controls.Add(this.cTxtName);
            this.Controls.Add(this.cLblName);
            this.Name = "diUserReg";
            this.Text = "用户注册窗体";
            this.ResumeLayout(false);
            this.PerformLayout();


        }
        
        #endregion

        private System.Windows.Forms.Label cLblName;
        private System.Windows.Forms.TextBox cTxtName;
        private System.Windows.Forms.Label cLblPwd;
        private System.Windows.Forms.TextBox cTxtPwd;
        private System.Windows.Forms.Label cLblConfirmPwd;
        private System.Windows.Forms.TextBox cTxtConfirmPwd;
        private System.Windows.Forms.Label cLblMobile;
        private System.Windows.Forms.TextBox cTxtMobile;
        private System.Windows.Forms.Label cLblMail;
        private System.Windows.Forms.TextBox cTxtMail;
        private System.Windows.Forms.Label cLblSafetyCode;
        private System.Windows.Forms.TextBox cTxtSafetyCode;
        private System.Windows.Forms.Label cLblSendCode;
        private System.Windows.Forms.Button cBtnReg;
        private System.Windows.Forms.Button cBtnCancel;

        
    }
}